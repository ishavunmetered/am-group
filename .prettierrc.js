module.exports = {
    singleQuote: true,
    semi: true,
    printWidth: 80,
    overrides: [
        {
            files: '*.scss',
            options: {
                singleQuote: true,
            },
        },
    ],

};