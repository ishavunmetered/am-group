import ClassName from "models/classname";
import styles from "./Loader.module.scss";

const Loader = ({ className }) => {
  return (
    <div className={`${styles.loaderwrapper} w-100`}>
      <div className={styles.loader_com}>
        <div>
          <img src="/video/am-logo.gif" alt="logo" />
        </div>
      </div>
    </div>
  );
};

export default Loader;
