import { useState } from "react";
import Container from "components/Container";
import Link from "next/link";
import Image from "next/image";

import Section from "components/Section";

import styles from "./Nav.module.scss";
import { CSSTransition } from 'react-transition-group';

const Nav = () => {
  const [isactive, setIsactive] = useState(false);
  const [navbar, setNavbar] = useState(false);
  const [isOpen, setOpen] = useState(false);

  const toggleDropdown = () => setOpen(!isOpen);

  const handleIsActive = () => {
    setIsactive(!isactive);
  };
  const changeBackground = () => {
    if (window.scrollY >= 80) {
      setNavbar(true);
    } else {
      setNavbar(false);
    }
  };

  if (typeof window !== "undefined") {
    window.addEventListener("scroll", changeBackground);
  }

  return (
    <Section data-scroll-section>
      {/* <Container> */}
      <header className={navbar ? `${styles.header} ${styles.header_fixed}` : ` ${styles.header}`}>
        <Container>
          <div className={`${styles.header_bottom} w-100`}>
            <div className={styles.header_flex}>
              <div className={styles.logo}>
                <Link href="/">
                  <a>
                    <img src="/icons/mb-header-orange.svg" />
                    {/* <Image src="/icons/header-logo.svg" width="100%" height="100%" layout="responsive" objectFit="contain" alt="icon"></Image> */}
                  </a>
                </Link>
              </div>
              <div className={styles.header_right}>
                <nav className={`${styles.navbar} ${styles.navbar_expand_lg}`}>
                  <div className={styles.navbar_collapse} id="navbarNav">
                    <ul className={styles.navbar_nav}>
                      <li className={`${styles.nav_item} ${styles.dropdown}`}>
                        <a data-bs-toggle="dropdown">Services</a>
                        <ul className={styles.dropdown_menu}>
                          <li>
                            <Link className={styles.dropdown_item} href="/services/social-media-management">
                              Social Media Management
                            </Link>
                          </li>
                          <li>
                            <Link className={styles.dropdown_item} href="/services/websites-development">
                              Websites Development
                            </Link>
                          </li>
                          <li>
                            <Link className={styles.dropdown_item} href="/services/content-creation">
                              Content Creation
                            </Link>
                          </li>
                          <li>
                            <Link className={styles.dropdown_item} href="/services/social-media-advertising">
                              Social Media Advertising
                            </Link>
                          </li>
                        </ul>
                      </li>

                      <li className={styles.nav_item}>
                        <Link className={styles.nav_link} href="/">
                          <a>Why Us?</a>
                        </Link>
                      </li>
                      <li className={styles.nav_item}>
                        <Link className={styles.nav_link} href="/">
                          <a>Success Stories</a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </nav>
              </div>
              <div className={styles.header_toggle}>
                <button onClick={handleIsActive} className={styles.header_toggle_icon}>
                  {navbar ? (
                    <img src="/icons/toggle-orange.svg" alt="icon" />
                  ) : (
                    <img src="/icons/toggle.svg" alt="icon" />
                  )}
                </button>
                <div className={styles.header_toggle_logo}>
                  {navbar ? (
                    <Link href="/">
                      <a><img src="/icons/mb-header-orange.svg" alt="icon" /></a>
                    </Link>
                  ) : (
                    <Link href="/">
                      <a><img src="/icons/mb-header-white.svg" alt="icon" /></a>
                    </Link>
                  )}
                </div>
              </div>
            </div>
          </div>
        </Container>
      </header>

      {/* {isactive ? ( */}
      <CSSTransition
        in={isactive}
        timeout={350}
        classNames="display"
        unmountOnExit
      >
        <header className={`${styles.header_trans} `}>
          <div className={styles.mobile_header}>
            <div className={styles.mobile_header_div}>
              <div className={styles.mobile_logo}>
                <Link className={styles.navbar_brand} href="/">
                  <img src="/icons/mb-header-orange.svg" />
                  {/* <Image src="/icons/header-logo.svg" layout="fill" alt="icon"></Image> */}
                </Link>
              </div>
              <div>
                <button className={styles.navbar_cut_btn} onClick={handleIsActive}>
                  <Image src="/icons/nav-cut.svg" width={20} height={20} alt="icon"></Image>
                </button>
              </div>
            </div>
            <div className="d-flex">
              <nav>
                <div>
                  <div className={styles.res_header_links}>
                    <ul className={styles.mobile_view_ul}>
                      <li className={styles.mobile_view_lists}>
                        <Link href="/">
                          <a onClick={handleIsActive}>Why Us?</a>
                        </Link>
                      </li>
                      <li className={`${styles.mobile_view_lists} ${styles.react_dropdown}`}>
                        <a className={styles.mobile_dropdown_header} onClick={toggleDropdown}>
                          Services
                        </a>
                        <ul className={`${styles.dropdown_body} ${isOpen && `${styles.open}`}`}>
                          <li className={styles.dropdown_item}>
                            <Link href="/services/social-media-management">
                              <a className={styles.mobile_dropitem} onClick={handleIsActive}>
                                Social Media Management
                              </a>
                            </Link>
                          </li>
                          <li className={styles.dropdown_item}>
                            <Link href="/services/websites-development">
                              <a className={styles.mobile_dropitem} onClick={handleIsActive}>
                                Websites Development
                              </a>
                            </Link>
                          </li>
                          <li className={styles.dropdown_item}>
                            <Link href="/services/content-creation">
                              <a className={styles.mobile_dropitem} onClick={handleIsActive}>
                                Content Creation
                              </a>
                            </Link>
                          </li>
                          <li className={styles.dropdown_item}>
                            <Link href="/services/social-media-advertising">
                              <a className={styles.mobile_dropitem} onClick={handleIsActive}>
                                Social Media Advertising
                              </a>
                            </Link>
                          </li>
                        </ul>
                      </li>
                      <li className={styles.mobile_view_lists}>
                        <Link href="/">
                          <a onClick={handleIsActive}>Success Stories</a>
                        </Link>
                      </li>
                      <li className={styles.mobile_view_lists}>
                        <Link href="/">
                          <a onClick={handleIsActive}>Grow My Business</a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
            </div>
            <div className={styles.res_social_links}>
              <Link href="https://www.facebook.com/affirmativemarketing/">
                <a target="_blank" rel="noreferrer">
                  <Image src="/icons/fb-icon.svg" width={21} height={21} alt="icon"></Image>
                </a>
              </Link>
              <Link href="https://www.instagram.com/_amgroup/">
                <a target="_blank" rel="noreferrer">
                  <Image src="/icons/insta-icon.svg" width={21} height={21} alt="icon"></Image>
                </a>
              </Link>
              <Link href="https://www.instagram.com/_amgroup/">
                <a target="_blank" rel="noreferrer">
                  <Image src="/icons/tiktok-icon.svg" width={21} height={21} alt="icon"></Image>
                </a>
              </Link>
              <p className={styles.contactus_btn}>
                Want More?
                <Link href="/">
                  <a>
                    <strong> Contact Us</strong>
                  </a>
                </Link>
              </p>
            </div>
          </div>
        </header>
      </CSSTransition>
      {/* ) : (
        ""
      )} */}
    </Section>
  );
};

export default Nav;
