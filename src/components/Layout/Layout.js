import { useRouter } from "next/router";
import { useEffect } from "react";
import { Helmet } from "react-helmet";
import { gsap } from "gsap/dist/gsap";
import { SplitText } from "gsap/dist/SplitText";

if (process.client) {
  gsap.registerPlugin(SplitText);
}
import { ScrollSmoother } from "gsap/dist/ScrollSmoother";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

if (process.client) {
  gsap.registerPlugin(ScrollTrigger, ScrollSmoother);
}
ScrollTrigger.defaults({
  toggleActions: "restart pause resume pause",
  scroller: "#smooth-content",
});


import styles from "./Layout.module.scss";

import useSite from "hooks/use-site";
import { helmetSettingsFromMetadata } from "lib/site";

import Nav from "components/Nav";
import Main from "components/Main";
import Footer from "components/Footer";

const Layout = ({ children }) => {
  const router = useRouter();
  const { asPath } = router;

  const { homepage, metadata = {} } = useSite();

  if (!metadata.og) {
    metadata.og = {};
  }

  metadata.og.url = `${homepage}${asPath}`;

  const helmetSettings = {
    defaultTitle: metadata.title,
    titleTemplate: process.env.WORDPRESS_PLUGIN_SEO === true ? "%s" : `%s - ${metadata.title}`,
    ...helmetSettingsFromMetadata(metadata, {
      setTitle: false,
      link: [
        {
          rel: "alternate",
          type: "application/rss+xml",
          href: "/feed.xml",
        },

        // Favicon sizes and manifest generated via https://favicon.io/

        {
          rel: "apple-touch-icon",
          sizes: "180x180",
          href: "/apple-touch-icon.png",
        },
        {
          rel: "icon",
          type: "image/png",
          sizes: "16x16",
          href: "/favicon.png",
        },
        {
          rel: "icon",
          type: "image/png",
          sizes: "32x32",
          href: "/favicon.png",
        },
        {
          rel: "manifest",
          href: "/site.webmanifest",
        },
      ],
    }),
  };

  ScrollTrigger.defaults({
    toggleActions: "restart pause resume pause",
    scroller: "#smooth-content",
    // pin: true,
    pinType: "fixed",
  });


  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger, ScrollSmoother);
    ScrollTrigger.refresh();

    let smoother = ScrollSmoother.create({
      wrapper: "#smooth-wrapper",
      content: "#smooth-content",
      effects: true,
      pin: true,
      pinType: "fixed",
      smooth: 2,
      smoothTouch: 0.1,
    });
    ScrollTrigger.refresh();
  }, []);

  useEffect(() => {
    var splitHide = new SplitText(".animate-title", { type: "words", linesClass: "linechild hide" });
    var split = new SplitText(".animate-title", {
      type: "words",
    });
    gsap.utils.toArray(split.words).forEach(function (elem) {
      ScrollTrigger.create({
        trigger: elem,
        onEnter: function () {
          gsap.fromTo(
            elem,
            { x: -150, autoAlpha: 0 },
            {
              duration: 1,
              x: 0,
              autoAlpha: 1,
              ease: "power4",
              overwrite: "auto",
            }
          );
        },
        onLeave: function () {
          gsap.fromTo(elem, { autoAlpha: 1 }, { autoAlpha: 0, overwrite: "auto" });
        },
        onEnterBack: function () {
          gsap.fromTo(
            elem,
            { x: 100, autoAlpha: 0 },
            {
              duration: 1,
              x: 0,
              autoAlpha: 1,
              ease: "power4",
              overwrite: "auto",
            }
          );
        },
        onLeaveBack: function () {
          gsap.fromTo(elem, { autoAlpha: 1 }, { autoAlpha: 0, overwrite: "auto" });
        },
      });
    });
  });

  // useEffect(() => {
  //   var splitHide = new SplitText(".partner-animate-title", { type: "words", linesClass: "linechild hide" });
  //   var split = new SplitText(".partner-animate-title", {
  //     type: "words",
  //   });
  //   gsap.utils.toArray(split.words).forEach(function (parelem) {
  //     ScrollTrigger.create({
  //       trigger: parelem,
  //       markers:true,
  //       onEnter: function () {
  //         gsap.fromTo(
  //           parelem,
  //           { x: -150, autoAlpha: 0 },
  //           {
  //             duration: 1,
  //             x: 0,
  //             autoAlpha: 1,
  //             ease: "power4",
  //             overwrite: "auto",
  //           }
  //         );
  //       },
  //       onLeave: function () {
  //         gsap.fromTo(parelem, { autoAlpha: 1 }, { autoAlpha: 0, overwrite: "auto" });
  //       },
  //       onEnterBack: function () {
  //         gsap.fromTo(
  //           parelem,
  //           { x: 100, autoAlpha: 0 },
  //           {
  //             duration: 1,
  //             x: 0,
  //             autoAlpha: 1,
  //             ease: "power4",
  //             overwrite: "auto",
  //           }
  //         );
  //       },
  //       onLeaveBack: function () {
  //         gsap.fromTo(parelem, { autoAlpha: 1 }, { autoAlpha: 0, overwrite: "auto" });
  //       },
  //     });
  //   });

  //   gsap.utils.toArray(".marketing-animate-title").forEach((elem) => {
  //     elem.split = new SplitText(elem, {
  //       type: "lines,words,chars",
  //       linesClass: "split-line",
  //     });

  //     gsap.from(elem.split.words, {
  //       scrollTrigger: {
  //         trigger: elem,
  //         toggleActions: "restart pause resume reverse",
  //         start: "50% 60%",
  //         overwrite: "auto",
  //       },
  //       duration: 0.1,
  //       ease: "power4",
  //       xPercent: 150,
  //       stagger: 0.2,
  //     });
  //   });
  // });

  useEffect(() => {
    gsap.utils.toArray(".desc-animate").forEach((elem) => {
      elem.split = new SplitText(elem, {
        type: "lines,words,chars",
        linesClass: "split-line",
      });

      gsap.from(elem.split.lines, {
        scrollTrigger: {
          trigger: elem,
          toggleActions: "restart pause resume reverse",
          start: "50% 60%",
        },
        duration: 0.6,
        autoAlpha: 0,
        ease: "circ.out",
        yPercent: 100,
        stagger: 0.2,
      });
    });
  });
  return (
    <>
      {/* <Helmet {...helmetSettings} /> */}

      <Nav />
      <div id="smooth-wrapper">
        <div id="smooth-content">
          <Main>{children}</Main>
          <Footer />
        </div>
      </div>
    </>
  );
};

export default Layout;
