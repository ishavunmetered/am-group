import React, { useState } from "react";
import { FiChevronRight, FiChevronDown } from "react-icons/fi";


export default function Businesssolutionsection(props) {
    const [shown, setIsShown] = useState(0);
    const handleIsActive = (value) => {
        setIsShown(value);
    };

    return (
        <section className="packeges_sec_main common_padding" id="marketing_packages">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-md-6">
                        <div className="packeges_sec_left common_title">
                            <p>
                                How We<span className="fw-bold"> Do It</span>
                            </p>
                            <h2>
                                Perfect Solutions For <br />
                                <span className="fw-bold">Your Business.</span>
                            </h2>
                        </div>
                    </div>

                    <div className="col-md-6 col-sm-12">
                        {props.props.map((item, index) => (
                            <div className={shown === index ? "bg_black packeges_sec_right" : "packeges_sec_right"} key={index}>
                                <div className="d-flex justify-content-between pb-2">
                                    <div className="flex_center common_title">
                                        <h3>
                                            {index + 1}. {item.title}
                                        </h3>
                                    </div>
                                    <div className="packege_right_btn" onClick={() => handleIsActive(index)}>
                                        {shown === index ?
                                            <FiChevronDown className="faq_icon" />
                                            : <FiChevronRight className="faq_icon" />}
                                    </div>
                                </div>
                                {shown === index && (
                                    <div className="packeges_bottom common_title">
                                        <p>{item.content}</p>
                                    </div>
                                )}
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </section>
    )
}