import { useEffect, useState } from "react";
import parse from 'html-react-parser';
import { FiChevronRight, FiChevronDown } from "react-icons/fi";


export default function Socialmediapackages() {
  const [packeges, setPackeges] = useState(null);
  const [isLoading, setLoading] = useState(false)
  const [shown, setIsShown] = useState(0);
  const handleIsActive = (value) => {
    setIsShown(value);
  };


  useEffect(() => {
    setLoading(true)
    fetch('https://wordpress-405916-2533555.cloudwaysapps.com/wp-json/wp/v2/smm-packages')
      .then((res) => res.json())
      .then((data) => {
        setPackeges(data)
        setLoading(false)
      })
  }, [])

  // const handleClick = (e) => {
  //   if (typeof window !== "undefined") {
  //     document.querySelector(".marketing_slider_column").classList.remove("marketing_slider_column_active");
  //   }

  //   e.currentTarget.classList.toggle("marketing_slider_column_active");
  // };

  return (

    <section className="packeges_sec_main common_padding" id="marketing_packages">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-md-6">
            <div className="packeges_sec_left common_title">
              <p>
                Our<span className="fw-bold"> Packages</span>
              </p>
              <h2>
                Social Media Marketing <br />
                <span className="fw-bold">Packages.</span>
              </h2>
            </div>
          </div>

          <div className="col-md-6 col-sm-12">
            {packeges &&
              packeges.map((item, index) => (
                <div className={shown === index ? "bg_black packeges_sec_right" : "packeges_sec_right"} key={index}>
                  <div className="d-flex justify-content-between pb-2">
                    <div className="flex_center common_title">
                      <h3>
                        {index + 1}. {item.title.rendered}
                      </h3>
                    </div>
                    <div className="packege_right_btn" onClick={() => handleIsActive(index)}>
                      {/* <AiOutlineRight className="faq_icon" /> */}
                      {shown === index ?
                        <FiChevronDown className="faq_icon" />
                        : <FiChevronRight className="faq_icon" />}
                    </div>
                  </div>
                  {shown === index && (
                    <div className="packeges_bottom common_title">
                      {parse(item.content.rendered)}
                      <div className="packeges_bottom_child">
                        <p>
                          Quantity of posts:&nbsp;
                          <span className="orange_color">{item.acf.smm_data.quantity_of_posts}</span>
                        </p>
                        <p>
                          Community Management:&nbsp;
                          <span className="orange_color">{item.acf.smm_data.community_management}</span>
                        </p>
                      </div>
                    </div>
                  )}
                </div>
              ))}
          </div>
        </div>
      </div>
    </section>
  );
}
