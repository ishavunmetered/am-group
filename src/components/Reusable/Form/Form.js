import React, { useState } from "react";
import styles from "./Form.module.scss";
import axios from 'axios';

export default function Form() {
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [service, setService] = useState('');
    const [company, setCompany] = useState('');
    const [message, setMessage] = useState('');
    const [formmessage, setFormmessage] = useState('');
    const [success, setSuccess] = useState(false);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);

    const sendformdata = async (e) => {
        e.preventDefault();
        setLoading(true);
        await axios.post(`https://wordpress-405916-2533555.cloudwaysapps.com/wp-json/contactusform/v1/contact?name=${name}&email=${email}&phone=${phone}&companyname=${company}&service=${service}&message=${message}`)
            .then((res) => {
                console.log(res.data)
                setLoading(false);
                setSuccess(true);
                setError(false);
                setFormmessage('Thankyou Your Enquiry has been Submitted, We will get in touch with you soon!');
                setName('');
                setPhone('');
                setEmail('');
                setService('');
                setCompany('');
                setMessage('');
            })
            .catch((err) => {
                console.log(err);
                setLoading(false);
                setError(true);
                setSuccess(false);
                setFormmessage('There was an Error submitting the form! Please try again ')

            })
    }

    return (
        <div className={styles.desktop_par_left_item}>
            <form className={`${styles.form_control} row`} onSubmit={sendformdata}>
                <div className={`${styles.name_input} col-md-6`}>
                    <input type="text" id="fullname" value={name} onChange={(e) => setName(e.target.value)} required />
                    <label htmlFor="fullname">Your Name</label>
                </div>
                <div className={`${styles.name_input} col-md-6`}>
                    <input type="number" id="phone" value={phone} onChange={(e) => setPhone(e.target.value)} required />
                    <label htmlFor="phone">Phone Number</label>
                </div>
                <div className="clearfix"></div>
                <div className={`${styles.name_input} col-md-6`}>
                    <input type="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    <label htmlFor="email">Your Email Address</label>
                </div>
                <div className={`${styles.name_input} col-md-6`}>
                    <input type="text" id="companyname" value={company} onChange={(e) => setCompany(e.target.value)} required />
                    <label htmlFor="companyname">Company Name</label>
                </div>
                <div className={`${styles.name_input} col-md-12`}>
                    <select name="services" id="services" value={service} onChange={(e) => setService(e.target.value)}>
                        <option value="" >Select Your Service</option>
                        <option value="social-media-management">Social Media Management</option>
                        <option value="website-development">Website Development</option>
                        <option value="content-creation">Content Creation</option>
                        <option value="social-media-advertising">Social Media Advertising</option>
                    </select>
                </div>
                <div className="clearfix"></div>
                <div className={`${styles.name_input} col-md-12`}>
                    <input type="text" id="messageforus" value={message} placeholder=" " onChange={(e) => setMessage(e.target.value)} required />
                    <label htmlFor="messageforus">Message</label>
                </div>
                <div className="clearfix"></div>
                <div className={styles.submit_btn}>
                    <button type="submit" className={`${styles.form_btn} common_btn`}>
                        Submit
                    </button>
                </div>
            </form>
            {success ? <div className="success-formmessage">  <p>{formmessage}</p> </div> : ''}
            {error ? <div className="error-formmessage">  <p>{formmessage}</p> </div> : ''}
        </div>
    )

}