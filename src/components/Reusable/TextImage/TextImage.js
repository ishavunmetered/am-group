import Image from "next/image";

export default function TextImage() {
  return (
    <section className="text_image">
      <div className="container">
        <div className="text_image_sec row">
          <div className="text_img_left col-lg-6 col-md-6 col-sm-12 common_title">
            <span>Social Media Management</span>
            <h1>
              Increase &<span>Boost</span> Your Traffic
            </h1>
            <p>
              Produce and automate a scroll-stopping social media page that speaks directly to an engaged audience.
              Forget likes or followers, think conversations that lead to conversions!
            </p>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12">
            <Image
              src="/images/text-image.jpg"
              width="100%"
              height="100%"
              layout="responsive"
              objectFit="contain"
            ></Image>
          </div>
        </div>
      </div>
    </section>
  );
}
