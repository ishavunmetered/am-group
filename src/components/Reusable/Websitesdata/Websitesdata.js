import React, { useState, useEffect } from "react";
import Link from "next/link"

var $ = require("jquery");
if (typeof window !== "undefined") {
    window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
    ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function Websitesdata() {
    const [web, setWeb] = useState(null);
    const [isLoading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        fetch('https://wordpress-405916-2533555.cloudwaysapps.com/wp-json/wp/v2/website')
            .then((res) => res.json())
            .then((data) => {
                setWeb(data)
                setLoading(false)
            })
    }, [])

    const web_slider = {
        margin: 20,
        // center: true,
        items: 4,
        loop: true,
        // nav: false,
        dots: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1.5,
                nav: false,
                dots: true,
                // center: true
            },
            600: {
                items: 2,
                // loop: true,
                dots: true,
            },
            1000: {
                items: 4,
                loop: true,
                dots: true,
            },
        },
    };
    if (isLoading) return <p>Loading...</p>
    return (
        <section className="websites_section common_padding">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-12 common_title text-left">
                        <h2>
                            The Websites <br /> We Build
                            <span className="orange_color fw-bold"> Are Keepers.</span>
                        </h2>
                    </div>
                    <div className="col-lg-5 offset-lg-1 col-md-5 offset-md-1 col-sm-12 common_title common_para text-left">
                        <p>
                            Our process ensures that every device is tested for a complete and faultless journey. Experience how
                            functionality meets aesthetics with just a few of our many projects.
                        </p>
                    </div>
                </div>
                <div className="clearfix"></div>
                <div className="row websites_section_columns">
                    <OwlCarousel responsiveClass="true" {...web_slider}>
                        {web && web.map((item, index) => (
                            <div className="website_column" key={index}>
                                <div
                                    className="website_column_image w-100"
                                    style={{
                                        backgroundImage: `url(${item.acf.layout_image})`,
                                    }}
                                ></div>
                                <button>
                                    <Link href={item.acf.website_link} passHref>
                                        <a target="_blank">{item.title.rendered}</a>
                                    </Link>
                                </button>
                            </div>
                        ))}
                    </OwlCarousel>
                </div>
            </div>
        </section>
    );
}