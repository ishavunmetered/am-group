import Image from "next/image";
import styles from "./Perfectsolution.module.scss"

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function PerfectSolutionsection(props) {

  const marketing_owl = {
    loop: false,
    margin: 20,
    // items: 3,
    nav: false,
    dots: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        dots: true,
        loop: true,
      },
      600: {
        items: 2,
        dots: true,
        loop: true,
      },
      1000: {
        items: 3,
        dots: false,
        loop: false,
      },
    },
  };
  return (
    <section className={`${styles.business_sol} common_padding pb-0`}>
      <div className="container">
        <div className="row">
          <div className={`${styles.business_text} common_title`}>
            <p className="orange_color">What We Do</p>
            <h2>
              Perfect Solutions For <br />
              <span className="fw-bold">Your Business.</span>
            </h2>
          </div>

          <div className="col-lg-12 common_padding business_carousel">
            <OwlCarousel responsiveClass="true" {...marketing_owl}>
              {props.props.map((item, index) => (
                <div className={styles.business_slider} key={index}>
                  <div className={styles.business_column}>
                    <span>
                      <Image src={item.image} alt="Icon" height={52} width={52} />
                    </span>
                    <h4>{item.title}</h4>
                    <p>{item.content}</p>
                  </div>
                </div>
              ))}
            </OwlCarousel>
          </div>
        </div>
      </div>
    </section>
  );
}
