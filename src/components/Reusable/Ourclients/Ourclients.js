import Image from "next/image";
import styles from "./Ourclients.module.scss";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function Ourclients(props) {

  const clientsSlider = {
    loop: true,
    margin: 20,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    smartSpeed: 700,
    nav: false,
    dots: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 4,
        dots: false,
        loop: true,
      },
      600: {
        items: 4,
        dots: false,
        loop: true,
      },
      1000: {
        items: 4,
        dots: false,
        loop: true,
      },
    },
  };

  return (
    <section className={`${styles.clients_slider_section} common_padding pt-0`}>
      <div className="container">
        <div className="row">
          <div className={`${styles.clients_title} common_title text-center`}>
            <h2>
              Our <span className="fw-bold"> Clients.</span>
            </h2>
          </div>

          <div className="col-md-12 col-sm-12">
            <div className={styles.clients_slider}>
              <OwlCarousel responsiveClass="true" {...clientsSlider}>
                {props.props.map((item, i) => (
                  <div key={i}>
                    <div className={styles.clients_clider_col}>
                      <Image src={item} width="100%" height={30} layout="responsive" objectFit="contain"></Image>
                    </div>
                  </div>
                ))}
              </OwlCarousel>
            </div>
            <div className={styles.clients_slider}>
              <OwlCarousel responsiveClass="true" {...clientsSlider}>
                {props.props.map((item, i) => (
                  <div key={i}>
                    <div className={styles.clients_clider_col}>
                      <Image src={item} width="100%" height={30} layout="responsive" objectFit="contain"></Image>
                    </div>
                  </div>
                ))}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
