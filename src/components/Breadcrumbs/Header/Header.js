import Container from "components/Container";

import styles from "./Header.module.scss";

const Header = ({ children }) => {
  return (
    <header className={styles.header} data-scroll-section>
      <Container>{children}</Container>
    </header>
  );
};

export default Header;
