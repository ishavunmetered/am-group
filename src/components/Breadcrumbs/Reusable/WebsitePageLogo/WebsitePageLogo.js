import Image from "next/image";
var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function WebsitePageLogo() {
  const logoSlider = {
    loop: false,
    margin: 20,
    // items: 4,
    nav: false,
    dots: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 4,
        dots: false,
        loop: false,
      },
      600: {
        items: 4,
        dots: false,
        loop: false,
      },
      1000: {
        items: 4,
        dots: false,
        loop: false,
      },
    },
  };

  const WebsitesLogo = [
    {
      logoImage: "/icons/websitespage/wordpress.svg",
    },
    {
      logoImage: "/icons/websitespage/mailchimp.svg",
    },
    {
      logoImage: "/icons/websitespage/hubshot.svg",
    },
    {
      logoImage: "/icons/websitespage/shopify.svg",
    },
  ];

  return (
    <div className="common_padding text-center pt-0">
      <div className="container">
        <OwlCarousel responsiveClass="true" {...logoSlider}>
          {WebsitesLogo.map((logo, index) => (
            <div key={index}>
              <Image src={logo.logoImage} width={130} height={70} alt="icon"></Image>
            </div>
          ))}
        </OwlCarousel>
      </div>
    </div>
  );
}
