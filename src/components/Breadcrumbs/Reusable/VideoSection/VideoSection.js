import styles from "./VideoSection.module.scss";
export default function VideoSection(props) {
  return (
    <section
      className={`${styles.evolution_section} ${styles.videography_section}`}
      style={{ backgroundImage: "url(/images/our-result.png)" }}
    >
      <div className={styles.evo_content}>
        <div className="container">
            <h2 className="">
              <span className="fw-bold">Videography</span>
            </h2>
            <div className={styles.evolution_video}>
             
              <a
                className="video-modal"
                data-toggle="modal"
                data-target="#videoModal"
                data-video="https://s3-us-west-2.amazonaws.com/s.cdpn.io/148422/Vienna-SD.mp4"
              >
                <img src="/icons/play-icon.svg" alt="Icon" />
              </a>
              <h4>Watch Video</h4>
          </div>
        </div>
      </div>
    </section>
  );
}
