import Image from "next/image";
import styles from "./Solutionforbusiness.module.scss";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function Solutionforbusiness(props) {
  const marketing_owl = {
    loop: false,
    margin: 20,
    // items: 3,
    nav: false,
    dots: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        dots: false,
        loop: true,
      },
      600: {
        items: 2,
        dots: false,
        loop: true,
      },
      1000: {
        items: 3,
        dots: false,
        loop: false,
      },
    },
  };

  const sol_business = [
    {
      icon: "/icons/audiance.svg",
      title: "Grow Your Audience",
      description:
        "Through audience research, excogitatum est super his, ut hominesuidam ignoti, vilitate ipsa pavendi coll igendos rumores per Antiochiae latera inarentur relaturi",
    },
    {
      icon: "/icons/engagement.svg",
      title: "Increase Engagement",
      description:
        "Excogitatum est super his, ut hominesuidam ignoti, vilitate ipsa pavendi coll igendos rumores per Antiochiae latera inarentur relaturi",
    },
    {
      icon: "/icons/channels.svg",
      title: "Automate Your Channels",
      description:
        "Excogitatum est super his, ut hominesuidam ignoti, vilitate ipsa pavendi coll igendos rumores per Antiochiae latera inarentur relaturi",
    },
  ];
  return (
    <section className={`${styles.business_sol} common_padding pb-0`}>
      <div className="container">
        <div className="row">
          <div className={`${styles.business_text} common_title`}>
            <p className="orange_color">What We Do</p>
            <h2>
              Perfect Soultions For <br />
              <span className="fw-bold">Your Business.</span>
            </h2>
          </div>

          <div className="col-lg-12 common_padding">
            <OwlCarousel responsiveClass="true" {...marketing_owl}>
              {sol_business.map((item, index) => (
                <div className={styles.business_slider} key={index}>
                  <div className={styles.business_column}>
                    <span>
                      <Image src={item.icon} alt="Icon" height={52} width={52} />
                    </span>
                    <h4>{item.title}</h4>
                    <p>{item.description}</p>
                  </div>
                </div>
              ))}
            </OwlCarousel>
          </div>
        </div>
      </div>
    </section>
  );
}
