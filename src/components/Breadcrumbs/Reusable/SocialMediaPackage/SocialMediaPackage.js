import { useEffect, useState } from "react";

export default function Socialmediapackages() {
  const [getall, setGetall] = useState();
  const [render, setrender] = useState("zz");
  let [active, setActive] = useState(false);

  const packeges = [
    {
      title: "Professional",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
      posts: "6 Weekly",
      management: "3 Hours Weekly",
    },
    {
      title: "Professional",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
      posts: "6 Weekly",
      management: "3 Hours Weekly",
    },
    {
      title: "Professional",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
      posts: "6 Weekly",
      management: "3 Hours Weekly",
    },
  ];

  //   const [one, setOne] = useState(false);
  //   const [two, setTwo] = useState(false);
  //   const [three, setThree] = useState(false);

  //   const [activeindex, setActiveindex] = useState(false);

  const handleClick = (e) => {
    if (typeof window !== "undefined") {
      document.querySelector(".marketing_slider_column").classList.remove("marketing_slider_column_active");
    }

    e.currentTarget.classList.toggle("marketing_slider_column_active");
  };

  return (
    <section className="packeges_sec common_padding" id="marketing_packages">
      <div className="container">
        <div className="packeges_top common_title">
          <p>
            Our<span className="fw-bold"> Packages</span>
          </p>
          <h2>
            Social Media Marketing <br />
            <span className="fw-bold">Packages.</span>
          </h2>
        </div>
        <div className="row pt-5">
          {packeges &&
            packeges.map((item, i) => (
              <div className="col-md-4" key={i}>
                <div className="packeges_bottom common_title">
                  <h3>
                    {i + 1}. {item.title}
                  </h3>
                  <p>{item.content}</p>
                  <div className="packeges_bottom_child">
                    <p>
                      Quantity of posts:&nbsp;
                      <span>{item.posts}</span>
                    </p>
                    <p>
                      Community Management:&nbsp;
                      <span>{item.management}</span>
                    </p>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </section>
  );
}
