import React, { useState } from "react";
import styles from "./Accordion.module.scss"
import { FiChevronRight, FiChevronDown } from "react-icons/fi";


export default function Accordion({ title, content, serialNo, index }) {
    const [isActive, setIsActive] = useState(false);

    return (
        <div className={`${styles.accordion_card_orange} accordion_card_black col-md-8 col-sm-12`}>
            <div className={`${styles.accordion_bg_orange} accordion_bg_black`} onClick={() => setIsActive(!isActive)}>
                <div className={styles.accordion_title}><h2><span className={`${styles.accor_no} accor_no_orage`}>{serialNo}.</span> {title}</h2></div>
                <div className={styles.accordion_btn}>
                    {isActive ?
                        <FiChevronDown className={styles.faq_icon} />
                        : <FiChevronRight className={styles.faq_icon} />}
                </div>
            </div>
            {isActive && <div className={styles.accordion_panel}>
                <p>{content}</p></div>}
        </div>

    )
}