import React, { useState, useRef, useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import Image from "components/Image";
import Layout from "components/Layout";
import styles from "../../styles/pages/Social-media-management.module.scss";
import VideoSection from "components/Reusable/VideoSection/VideoSection";

const ContentCreation = () => {

  const gallerytabs = [
    {
      title: "Food & Drink",
      imageGallery: [
        {
          bgImage: "/images/gallery/food1.png",
        },
        {
          bgImage: "/images/gallery/food2.png",
        },
        {
          bgImage: "/images/gallery/food3.png",
        },
        {
          bgImage: "/images/gallery/food4.png",
        },
      ],
    },
    {
      title: " Product",
      imageGallery: [
        {
          bgImage: "/images/gallery/food1.png",
        },
        {
          bgImage: "/images/gallery/food2.png",
        },
        {
          bgImage: "/images/gallery/food3.png",
        },
        {
          bgImage: "/images/gallery/food4.png",
        },
      ],
    },
    {
      title: "Venue",
      imageGallery: [
        {
          bgImage: "/images/gallery/food3.png",
        },
        {
          bgImage: "/images/gallery/food4.png",
        },
        {
          bgImage: "/images/gallery/food1.png",
        },
        {
          bgImage: "/images/gallery/food2.png",
        },

      ],
    },
    {
      title: "Services",
      imageGallery: [
        {
          bgImage: "/images/gallery/food3.png",
        },
        {
          bgImage: "/images/gallery/food4.png",
        },
        {
          bgImage: "/images/gallery/food1.png",
        },
        {
          bgImage: "/images/gallery/food2.png",
        },

      ],
    },
    {
      title: "Food",
      imageGallery: [
        {
          bgImage: "/images/gallery/food1.png",
        },
        {
          bgImage: "/images/gallery/food2.png",
        },
        {
          bgImage: "/images/gallery/food3.png",
        },
        {
          bgImage: "/images/gallery/food4.png",
        },
      ],
    },
  ];

  return (
    <>
      <Head>
        <title>Services | Content Creation</title>
        <link rel="shortcut icon" href="/favicon.png" />

      </Head>
      <Layout>
        <section className="banner services_banner common_padding pb-0">
          <div className="container">
            <div className="banner_content">
              <div className="row align-items-center">
                <div className="col-lg-8 col-md-8 col-md-12 services_content_left common_title">
                  <p>
                    Content<span className="fw-bold"> Creation</span>
                  </p>
                  <h1>
                    Our Captured
                    <br /> <span className="orange_color fw-bold">Moments</span>
                  </h1>
                </div>
                <div className="col-lg-4 col-md-4 col-md-12 services_content_right">
                  <p>
                    We don’t shoot to show what things look like, we shoot with an intention, evoking emotion and
                    procuring interest with every image or video.
                  </p>
                </div>
              </div>
            </div>
            <div className="banner_image_column">
              <img src="/images/content-creation.png"></img>
            </div>
          </div>
        </section>
        <section className="projects_section common_padding photography_tabs_section" id="photography_tabs_section">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12 common_title text-center">
                <h2 className="fw-bold">Photography</h2>
                <p>
                  If content is what you need, then AM Group has you covered. Employ our creative minds, state of the
                  art equipment & specialised content creators to capture the true essence & inner workings of your
                  brand.
                </p>
              </div>

              <div className="col-md-12">
                <div className="project_tabs">
                  <ul className="nav nav-tabs" id="myTab" role="tablist">
                    {gallerytabs.map((item, i) => (
                      <li className="nav-item" role="presentation" key={i}>
                        <a
                          className="nav-link"
                          id="Websites-tab"
                          data-toggle="tab"
                          href={`#tab${i + 1}`}
                          role="tab"
                          aria-controls="websites"
                          aria-selected="false"
                        >
                          {item.title}
                        </a>
                      </li>
                    ))}
                  </ul>
                  <div className="tab-content" id="myTabContent">
                    {gallerytabs.map((item, i) => (
                      <div
                        key={i}
                        className={i == 0 ? "tab-pane fade show active" : "tab-pane fade"}
                        id={`#tab${i + 1}`}
                        role="tabpanel"
                        aria-labelledby="All-tab"
                      >
                        <div className="row">
                          {item.imageGallery.map((myitem, index) => (
                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-6 photography_tabs_column" key={index}>
                              <div className="photography_column">
                                <a>
                                  <img src={myitem.bgImage} alt="Image" />
                                </a>
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <VideoSection />

        <div className={`${styles.content_sec} common_padding pt-0`}>
          <div className="container">
            <div className={`${styles.content_main} row`}>
              <div className={`${styles.content_main_left} col-md-6 col-sm-6 common_title `}>
                <h2>
                  Content  <br />
                  <span className="fw-bold">Packages</span>
                </h2>
                <p>
                  We have the experience to make the next shoot your best! Reach out below to learn more about our photography and videography packages.
                </p>
                <Link href="/">
                  <a className="common_btn">Get In Touch</a>
                </Link>
              </div>
              <div className={`${styles.content_main_right} ${styles.hideon_mobile} col-md-6 col-sm-6`}>
                <div style={{ backgroundImage: `url("/images/contentimg1.png")` }} className={styles.left_image}></div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default ContentCreation;
