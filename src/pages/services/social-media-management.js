import React, { useState } from "react";
import Link from "next/link";
import Image from "components/Image";
import Head from "next/head";
import Layout from "components/Layout";
import axios from 'axios';
import Socialmediapackages from "components/Reusable/SocialMediaPackage/SocialMediaPackage";
import TextImage from "components/Reusable/TextImage/TextImage";
import styles from "../../styles/pages/Social-media-management.module.scss";
import Ourclients from "components/Reusable/Ourclients/Ourclients";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import PerfectSolutionsection from "components/Reusable/Perfectsolutionsection/Perfectsolutionsection";

const Socialmediamanagement = ({ data }) => {
  const [pagedata, setPagedata] = useState();
  // const [loading, setLoading] = useState(false);
  // const [shown, setIsShown] = useState(0);
  // const handleIsActive = (value) => {
  //   setIsShown(value);
  // };

  useState(() => {
    setPagedata(data.acf);
  });

  const logoSlider = {
    loop: false,
    margin: 20,
    // items: 4,
    autoPlay: true,
    speed: 200,
    nav: false,
    dots: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 3,
        dots: false,
        loop: false,
      },
      600: {
        items: 3,
        dots: false,
        loop: false,
      },
      1000: {
        items: 5,
        dots: false,
        loop: false,
      },
    },
  };
  const managementLogo = [
    {
      logoImage: "/business-logo/logos/facebook.png",
    },
    {
      logoImage: "/business-logo/insta.png",
    },
    {
      logoImage: "/business-logo/logos/SVG/linkedin.png",
    },
    {
      logoImage: "/business-logo/shopify-partners.png",
    },
    {
      logoImage: "/business-logo/logos/SVG/sprout.svg",
    },
  ];
  return (
    <>
      <Head>
        <title>Services | Social Media Management</title>
        <link rel="shortcut icon" href="/favicon.png" />

      </Head>
      <Layout>
        <section className="banner services_banner common_padding">
          <div className="container">
            <div className="banner_content">
              <div className="row align-items-center">
                <div className="col-lg-8 col-md-8 col-md-12 services_content_left common_title">
                  <p>
                    Social Media<span className="fw-bold"> Management</span>
                  </p>
                  <h1>
                    Increase &<span className="orange_color fw-bold"> Boost</span>
                    <br />
                    Your Traffic
                  </h1>
                </div>
                <div className="col-lg-4 col-md-4 col-md-12 services_content_right">
                  <p>
                    Produce and automate a scroll-stopping social media page that speaks directly to an engaged
                    audience. Forget likes or followers, think conversations that lead to conversions!
                  </p>
                </div>
              </div>
            </div>
            <div className="banner_image_column">
              <img src="/images/text-image.jpg"></img>
            </div>
          </div>
        </section>

        <div className="common_padding text-center pt-0">
          <div className="container">
            <OwlCarousel responsiveClass="true" {...logoSlider}>
              {managementLogo.map((logo, index) => (
                <div className="logo_slider" key={index}>
                  <Image src={logo.logoImage} width="100%" height="100%" layout="responsive" objectFit="contain" alt="icon"></Image>
                </div>
              ))}
            </OwlCarousel>
          </div>
        </div>

        <Socialmediapackages />

        <PerfectSolutionsection props={pagedata && pagedata.perfect_solutions} />

        <div className={`${styles.content_sec} common_padding pt-0`}>
          <div className="container">
            <div className={`${styles.content_main} row`}>
              <div className={`${styles.content_main_right} col-md-6 col-sm-6`}>
                <div className={styles.left_image1}>
                  <img src="/images/insta-img.png" />
                </div>
              </div>
              <div className={`${styles.content_main_left} col-md-6 col-sm-6`}>
                <div className={`${styles.content_text_right} common_title `}>
                  <h2>
                    Content
                    <span className="fw-bold"> Calendar.</span>
                  </h2>
                  <p>
                    With a dedicated Social Media Account Manager, they’ll work closely with our network of content
                    creators and graphic designers to execute and plan an effective social media marketing strategy that
                    is straight forward, automated and on-brand. {"\n \n"} We’ll produce and automate a scroll-stopping
                    social media page that speaks directly to an engaged audience, so you’ll never need to put your
                    social media strategy on the back-burner again.
                  </p>
                  <Link href="/">
                    <a className="common_btn">Let's Chat</a>
                  </Link>

                </div>
              </div>
            </div>

            <div className={`${styles.content_main} row`}>
              <div className={`${styles.content_main_left} col-md-6 col-sm-6`}>
                <div className={`${styles.content_text_left} common_title `}>
                  <h2>
                    Community
                    <span className="fw-bold"> Management.</span>
                  </h2>
                  <p>
                    In basic terms – We’ll make life SO much easier for you! We’ll spend a minimum of 100 minutes per
                    week on your platforms, engaging with, liking and commenting on your audience’s content and other
                    relevant brands/individuals. Algorithm’s are constantly changing, so we’ll adapt community
                    management techniques to align with the current social media climate. This aspect of SMM will help
                    grow your audience and keep your community engaged and entertained.
                  </p>
                  <Link href="/">
                    <a className="common_btn">Let's Chat</a>
                  </Link>
                </div>
              </div>
              <div className={`${styles.content_main_right} ${styles.left_order} col-md-6 col-sm-6`}>
                <div className={styles.left_image}>

                  <img src="/images/contentimg1.jpg" />
                </div>
              </div>
            </div>

            <div className={`${styles.content_main} row`}>
              <div className={`${styles.content_main_right} col-md-6 col-sm-6`}>
                <div style={{ backgroundImage: `url("/images/contentimg2.png")` }} className={styles.left_image3}>
                  <button>
                    <Image src="/clients-logo/minto.svg" width={98} height={32}></Image>
                  </button>
                </div>
                {/* <img src="/images/contentimg2.png" /> */}
              </div>
              <div className={`${styles.content_main_left} ${styles.cont_last_sec} col-md-6 col-sm-6`}>
                <div className={`${styles.content_text_right} common_title `}>
                  <h2>
                    Reporting &
                    <span className="fw-bold"> Analytics.</span>
                  </h2>
                  <p>
                    AM GROUP understands and keeps on top of the ever-evolving social media marketing landscape, so you
                    don’t have to. We’ll create a tailored marketing strategy for your brand that will drive traffic to
                    your website. Our clients can login to an online portal at anytime to see live reporting on their
                    marketing results. Imagine having 24/7 access to see where every dollar is spent!
                  </p>
                  <Link href="/">
                    <a className="common_btn">Let's Chat</a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Ourclients props={pagedata && pagedata.our_clients} />

        <section className={`${styles.certified_Section} common_padding pt-0`}>
          <div className="container">
            <div className={`${styles.ourwork_main} row`}>
              <div className="col-md-6 col-sm-12">
                <div className="common_title text-left w-100">
                  <h2 className="text-capitalize">
                    A Glance at
                    <br />
                    <span className="fw-bold"> Our Work</span>
                  </h2>
                  <p>
                    Excogitatum est super his, ut homines uid psa pavendi coll igendos rumores per Ant iochiae latera
                    inatur relaturiExcogitatume st super his ut homines uida
                  </p>
                  <Link href="/">
                    <a className="common_btn">Case Studies</a>
                  </Link>
                </div>
              </div>
              <div className={`${styles.ourwork_right} col-md-6 col-sm-12`}>
                <div className={styles.ourwork_image}></div>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    </>
  );
};

export async function getServerSideProps() {
  const data = await axios
    .get(
      `https://wordpress-405916-2533555.cloudwaysapps.com/wp-json/acf/v3/pages/267`
    )
    .then((res) => ({
      data: res.data,
    }))
    .catch((err) => {
      console.log(err);
    });

  return {
    props: data,
  };
}

export default Socialmediamanagement;
