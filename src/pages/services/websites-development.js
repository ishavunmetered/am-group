import React, { useState, useRef, useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import Image from "components/Image";
import axios from "axios";
import Layout from "components/Layout";
import Accordion from "components/Accordion/Accordion";
import Businesssolutionsection from "components/Reusable/Businesssolutionsection/Businesssolutionsection";
import Websitesdata from "components/Reusable/Websitesdata/Websitesdata"
import WebsitePageLogo from "components/Reusable/WebsitePageLogo/WebsitePageLogo";

const Websites = ({ data }) => {
  const [pagedata, setPagedata] = useState();
  // const [shown, setIsShown] = useState(0);
  // const [questions, setIsQuestions] = useState(null);

  useState(() => {
    setPagedata(data.acf);
  });

  const faqs = [
    {
      serialNo: "01",
      title: " How long does it take to see results",
      content:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      serialNo: "02",
      title: "Do I have to be local to work with you?",
      content:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      serialNo: "03",
      title: "Are there any hidden agency fees?",
      content:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      serialNo: "04",
      title: "How profitable are Facebook ads?",
      content:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      serialNo: "05",
      title: "How much do I need to spend on Facebook?",
      content:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      serialNo: "06",
      title: "Will I be locked in a contract?",
      content:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      serialNo: "07",
      title: "Does AM Group create my content?",
      content:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
  ];

  return (
    <>
      <Head>
        <title>Services | Websites Development</title>
        <link rel="shortcut icon" href="/favicon.png" />
      </Head>
      <Layout>
        <section className="banner services_banner common_padding">
          <div className="container">
            <div className="banner_content">
              <div className="row align-items-center  ">
                <div className="col-lg-8 col-md-8 col-md-12 common_title">
                  <p>
                    Website Design <span className="fw-bold">& Development</span>
                  </p>
                  <h1>
                    We Create Sites
                    <br /> <span className="orange_color fw-bold">That Convert</span>
                  </h1>
                </div>
                <div className="col-lg-4 col-md-4 col-md-12 services_content_right">
                  <p>
                    Considering it's your sales person that never sleeps, we specialise in building beautiful and
                    functional websites customised to what’s needed to help your business grow.
                  </p>
                </div>
              </div>
            </div>
            <div className="banner_image_column">
              <img src="/images/websites.png"></img>
            </div>
          </div>
        </section>

        <WebsitePageLogo />
        <Businesssolutionsection props={pagedata && pagedata.how_we_do_it.card} />
        <Websitesdata />

        <section className="faq_sec faq_sec_orange common_padding">
          <div className="container">
            <div className="faq_top_white common_title">
              <p>
                Why Choose<span className="fw-bold"> AM Group</span>
              </p>
              <h2>
                Frequently Asked <span className="fw-bold">Questions.</span>
              </h2>
            </div>
            <div className="faq_main faq_main_black">
              {faqs.map(({ title, content, serialNo }, index) => (
                <Accordion title={title} content={content} serialNo={serialNo} index={index} key={index} />
              ))}
            </div>
            <div className="faq_btn_main">
              <Link href="/">
                <a className="faq_btn faq_btn_white">Get Started</a>
              </Link>
            </div>
          </div>
        </section>
      </Layout>
    </>
  );
};

export async function getServerSideProps() {
  const data = await axios
    .get(
      `https://wordpress-405916-2533555.cloudwaysapps.com/wp-json/acf/v3/pages/192`
    )
    .then((res) => ({
      data: res.data,
    }))
    .catch((err) => {
      console.log(err);
    });

  return {
    props: data,
  };
}

export default Websites;
