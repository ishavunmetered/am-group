import NextApp from "next/app";
import Head from "next/head";
import Image from "next/image";
import Loader from "components/Loader";
import { gsap } from "gsap/dist/gsap";
import { SplitText } from "gsap/dist/SplitText";
import { SiteContext, useSiteContext } from "hooks/use-site";
import { SearchProvider } from "hooks/use-search";

import { getSiteMetadata } from "lib/site";
import { getRecentPosts } from "lib/posts";
import { getCategories } from "lib/categories";
import { getAllMenus } from "lib/menus";

import "styles/globals.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import variables from "styles/_variables.module.scss";
import { useEffect, useRef, useState } from "react";
import { useRouter } from "next/router";

if (process.client) {
  gsap.registerPlugin(SplitText);
}
function App({ Component, pageProps = {}, metadata, recentPosts, categories, menus }) {
  const router = useRouter();
  const id = useRef(null);
  const logoRef = useRef();
  const [loading, setLoading] = useState(false);
  const [preload, setIsPreload] = useState(true);


  useEffect(() => {
    const loadData = async () => {
      await new Promise((r) => setTimeout(r, 5000));
      setIsPreload(!preload);
    };
    loadData();
  }, []);

  useEffect(() => {
    const handleStart = (url) => {
      url !== router.pathname ? setLoading(true) : setLoading(false);
    };

    const handleComplete = (url) => setLoading(false);

    router.events.on("routeChangeStart", handleStart);
    router.events.on("routeChangeComplete", handleComplete);
    router.events.on("routeChangeError", handleComplete);
  }, [router]);

  const site = useSiteContext({
    metadata,
    recentPosts,
    categories,
    menus,
  });

  // useEffect(() => {
  //   var split = new SplitText(".p-one", { type: "words" });
  //   gsap.from(split.words, { duration: 0.5, opacity: 0, filter: "blur(1px)", y: -10, stagger: 0.1 });
  // });

  return (
    <>
      {preload || loading ? (
        <Loader />
      ) : (
        <SiteContext.Provider id="scroll_sec" value={site} ss data-scroll-conatiner>
          <SearchProvider>
            <Component {...pageProps} />
          </SearchProvider>
        </SiteContext.Provider>
      )}
    </>
  );
}

App.getInitialProps = async function (appContext) {
  const appProps = await NextApp.getInitialProps(appContext);

  const { posts: recentPosts } = await getRecentPosts({
    count: 5,
    queryIncludes: "index",
  });

  const { categories } = await getCategories({
    count: 5,
  });

  const { menus = [] } = await getAllMenus();

  return {
    ...appProps,
    metadata: await getSiteMetadata(),
    recentPosts,
    categories,
    menus,
  };
};

export default App;
